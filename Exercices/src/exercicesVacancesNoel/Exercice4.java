package exercicesVacancesNoel;

import java.util.Arrays;
import java.util.Scanner;

public class Exercice4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int size = 4; 
		int[] values = new int[size];
	    for (int i = 0; i < size; i++) {
	    	System.out.print("Valeur " + (i+1) + "= ");
	    	values[i] =  sc.nextInt();
		}
	    Arrays.sort(values);
	    if(values[0] == values[size - 1]) {
	    	System.out.print("Les nombres sont identiques");
	    }
	    else {
	    	System.out.print("Les nombres ne sont pas identiques");
	    }
	}
}
