package exercicesVacancesNoel;

import java.util.Scanner;

public class Exercice6 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] values = new int[3];
		for (int i = 0; i < 3; i++) {
			System.out.print("Valeur "+(i+1) + " = ");
			values[i] = sc.nextInt();
		}
		if(values[0] == values[1] && values[1] == values[2]) {
			System.out.print("Tous les nombres sont �gaux");
		}
		else if(values[0] != values[1] && values[1] != values[2]) {
			System.out.print("Tous les nombres sont diff�rents");
		}
		else {
			System.out.print("Ni tous ni �gaux ni diff�rents");
		}
	}
}
