package exercicesVacancesNoel;

import java.util.Arrays;

public class Exercice3 {

	public static void main(String[] args) {
		String sentence = "python is more propular than java than python than java";
		String[] words = sentence.split(" ");
		String result = "";
		for (int i = 0; i < words.length; i++) {
			if(words[i].equals("python")) {
				result += "java ";
			}
			else if(words[i].contains("java")) {
				result += "python ";
			}
			else {
				result += words[i] + " ";
			}
		}
		System.out.println("La phrase de d�part : " + sentence);
		System.out.println("La phrase transform�e : " + result);
	}
}
