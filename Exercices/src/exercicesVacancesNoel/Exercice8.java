package exercicesVacancesNoel;

import java.util.Arrays;
import java.util.Scanner;

public class Exercice8 {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int userChoice = -1;
		double rate = 0.89;
		while(userChoice != 2) {
			System.out.println("Fa�tes votre choix : ");
			System.out.println("1. Convertir en euro");
			System.out.println("2. Quitter");
			userChoice = scn.nextInt();
			if(userChoice == 2) {
				System.out.println(" A bient�t");
			}
			else if(userChoice == 1) {
				System.out.println("Quel est le montant en dollar : ");
				double amount = scn.nextDouble();
				System.out.println(amount + "$ = " + amount * rate + "�");
			}
			else {
				System.out.println("Les valeurs possibles sont : 1 ou 2");
			}
		}
	}
}
