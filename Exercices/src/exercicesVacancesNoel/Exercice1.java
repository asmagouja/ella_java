package exercicesVacancesNoel;

import java.util.Arrays;

public class Exercice1 {

	public static void main(String[] args) {
		int[] array1 = {1, 2 , 5};
		int[] array1Copy = Arrays.copyOf(array1, array1.length);
		System.out.println("Tableau 1");
		String values = "";
		for (int i = 0; i < array1.length; i++) {
			values += " " + array1[i];
		}
		System.out.println(values);
		System.out.println("Copie du Tableau 1");
		values = "";
		for (int i = 0; i < array1Copy.length; i++) {
			values += " " + array1Copy[i];
		}
		System.out.println(values);
	}

}
