package exercicesVacancesNoel;

import java.util.Arrays;

public class Exercice2 {

	public static void main(String[] args) {
		int[] array1 = {1, 2, 1, 4, 5, 1 , 3, 3, 2, 5};
		String values = "";
		Arrays.sort(array1);
		int last = -1;
		boolean founded = false;
		for (int i = 0; i < array1.length; i++) {
			if(array1[i] == last) {
				if(!founded) {
					founded = true;
					values += " " + last;
				}
			}
			else {
				last = array1[i];
				founded = false;
			}
		}
		System.out.println(values);
	}

}
