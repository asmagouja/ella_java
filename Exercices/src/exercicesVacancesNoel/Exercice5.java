package exercicesVacancesNoel;

import java.util.Scanner;

public class Exercice5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double distance;
		int hours;
		int minutes;
		int seconds;
		System.out.print("Distance : ");
		distance = sc.nextDouble();
		System.out.print("Heures : ");
		hours = sc.nextInt();
		System.out.print("Minutes : ");
		minutes = sc.nextInt();
		System.out.print("Secondes : ");
		seconds = sc.nextInt();
		int totalSeconds = hours * 3600 + minutes * 60 + seconds;
		System.out.println("Vitesse (m/s) : " + distance/totalSeconds);
		double totalHours = totalSeconds / 3600;
		double kmPerHours = (distance/1000) / totalHours;
		System.out.println("Vitesse (km/h) : " + kmPerHours);
		System.out.println("Vitesse (mile/h) : " + (kmPerHours / 1.609344));
	}
}
